var express = require('express');
var request = require('request');
var router = express.Router();

/* GET home page. */
/*
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

module.exports = router;
*/
router.get('/', function(req, res, next) {
  requestAPI(function(resp, error) {
    if (resp === "Error APP") {
      res.render('error', {message:'Error al conectar con la API', error:error});
    } else {
      var title = "Test 4YouSee";
      body=JSON.stringify(resp);
      var data = JSON.parse(resp);
      var types = [];
      var categories = [];
      for (var i = 0; i < data.length; i++) {
        categories.push(data[i].category);
        types.push(data[i].type);
      }
      var typesUniques = [...new Set(types)];
      var categoriesUnique = [...new Set(categories)];
      res.render('index', {title:title, data:data, types:typesUniques, categories:categoriesUnique});
    }
  });
});

module.exports = router;


/*
Funcion encargada del llamdo a la API de 4YouSee y de devolver la respuesta
*/
function requestAPI(callback, error) {
  var options = {
    uri : 'https://private-7cf60-4youseesocialtest.apiary-mock.com/timeline',
    method : 'GET'
  };
  request(options, function (error, response, body) {
    if (!error && response.statusCode == 200) {
      res = body;
    } else {
      res = 'Error APP';
    }
    callback(res, error);
  });
}