# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Repositorio con el codigo del ejercicio propuesto para el cargo de desarrollador
Este proyecto consume los servicios del api propuesta para el ejercicio usando nodejs con el plugin expresjs
Para el front-end se uso jade (https://www.npmjs.com/package/jade) un framework que ayuda a la construccion de html de p�ginas web
Para el filtro de categorias y tipos se uso el plugin MixItUp (https://www.kunkalabs.com/mixitup/) en su version demo
Para la petici�n web para el consumo de la API se usa el cliente Request (https://github.com/request/request)

### How do I get set up? ###

* Summary of set up
* Dependencies
Para el funcionamiento del proyecto se debe tener previamente instalado nodejs (https://nodejs.org/) y expressjs (http://expressjs.com/)
* Deployment instructions
Despues de tener instalado las dependencias se debe ejecutar el comando
npm install
El cual se encarga de instalar las herramientas necesarias para la ejecucion del proyecto
El siguiente comando a ejecutar es
npm start
El cual se encarga de iniciar el servidor web para el funcionamiento correcto del proyecto
Finalmente para ver el proyecto se debe acceder a la pagina
http://localhost:3000/
En la cual se puede observar el proyecto con los filtros implementados